extends Spatial

var path = []
var begin = Vector3()
var end = Vector3()
var draw_path = true
var m = SpatialMaterial.new()

var capture = true

func _enter_tree():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	base.label = $Label
	
#func _ready():
#	begin = base.pA.global_transform.origin
#	end = base.pB.global_transform.origin
	

func _process(delta):
	if Input.is_action_just_pressed("debug_c"):
		Engine.time_scale = 1
	elif Input.is_action_just_pressed("debug_d"):
		Engine.time_scale = 0.5
	elif Input.is_action_just_pressed("ui_cancel"):
		capture = !capture
		if capture:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
#		begin = get_node("player").global_transform.origin
#		end = get_node("enemy").global_transform.origin
#		_update_path()
#	elif Input.is_action_just_pressed("debug_b"):
#		pass
#


func _update_path():
	var p = base.navig.get_simple_path(begin, end, true)
	path = Array(p) # Vector3array too complex to use, convert to regular array
	path.invert()
	set_process(true)

	if draw_path:
		var im = get_node("draw")
		im.set_material_override(m)
		im.clear()
		im.begin(Mesh.PRIMITIVE_POINTS, null)
		im.add_vertex(begin)
		im.add_vertex(end)
		im.end()
		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		for x in p:
			im.add_vertex(x)
		im.end()