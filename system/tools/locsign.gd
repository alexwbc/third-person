extends Sprite3D

var timer = 2
var color = Color(0,0,1,1)

func _enter_tree():
	set_modulate(color)

func _process(delta):
	timer -= 1*delta
	if timer <=0:
		queue_free()