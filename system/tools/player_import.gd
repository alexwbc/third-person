tool
extends EditorScenePostImport

func post_import(scene):
	for obj in scene.get_children():
		var skeleton = obj.get_node("Skeleton")
		if skeleton != null:
			_on_skeleton(skeleton)
		elif obj.get_class() == "AnimationPlayer":
			_on_animplayer(obj)
		
				
#		if obj.name.find("ref-camera") != -1:
#			var rot = obj.rotation_degrees
#			var place_holder = Position3D.new()
#			place_holder.transform.origin = obj.transform.origin
#
#			rot += Vector3(0,-180,180)
#			place_holder.rotation_degrees = rot
#			place_holder.name = obj.name
#			scene.add_child(place_holder)
#			place_holder.set_owner(scene) 
#			obj.queue_free()
#		if obj.get_class() == "MeshInstance":
#			obj.mesh.surface_set_material(0,materialstroke)
	

	return scene # remember to return the imported scene


func _on_animplayer(player):
	player.name = "anim"
	for an in player.get_animation_list():
		var anim = player.get_animation(an)
		print( anim.get_track_count())
		var track_kill = anim.find_track("rig")
		anim.remove_track(track_kill)
		print( anim.get_track_count())
#		print(anim.track_get_path(track_kill) )
#		var track_c = anim.get_track_count()
#		anim.remove_track("")
#		for t in range(track_c):
#			anim.remove_track(track_c)
#			track_c -= 1
#		print(track_c)
#		print(anim.find_track("rig").name )


		if an.find("-loop") != -1:
			player.rename_animation(an, an.replace("-loop", "" ))
#			print(player.get_animation(an).name)
#			print(str("before: ", an))
#			print(str(an.replace("-loop", "" )))
		
#		remove_track( int idx 
	if player.has_animation("idle"):
		player.set_autoplay("idle")

func _on_skeleton(skeleton):
	pass
