extends KinematicBody

var player_dist = Vector3()

var eye = RayCast

var g = -40
var vel = Vector3()
var phy_plug = null#bridge between pyshics and normal process (ie: when basic process request a jump)

#const MAX_SPEED = 12
const MAX_SPEED = 8
const JUMP_SPEED = 20

var dir = Vector3()

const ACCEL= 9
const DEACCEL= 9
const AIR_ACCEL = 1
const MAX_SLOPE_ANGLE = 30


var anim_old = "idle"
var anim_new = ["idle",false]
var anim_tree

var body
var skeleton = Skeleton



var statedb = 	{
				"idle":0
				,"chase":1
#				,"jump":2
				,"onair":3
				,"standup":4#after a falling,
#				,"jump":1
#				,"fall":2
#				,"attack":3
				}
var state = 0
var state_new = 0


const AI_MIN_SPEED_CHASE = 5
var ai_db ={"unaware":0
			,"chase":1
			,"follow_path":2
			
			}
var ai_state = 0

var ai_last_target = Vector3()

#path finding
var path = []
var path_cur = 0
#var begin = Vector3()
#var end = Vector3()


#pathfind debug
var draw_path = true
var m = SpatialMaterial.new()


var timer = [0,3]

#debug stuff
var debug_stop = false
var material = [SpatialMaterial,SpatialMaterial]
var mesh = MeshInstance
var mesh_switch = [false,1]

func _set_debug_stuff():
	for aaa in body.get_children():
		if aaa.get_class() == "Spatial":
			for bbb in aaa.get_children():
				if bbb.get_class() == "Skeleton":
					for ccc in bbb.get_children():
						if ccc.get_class() == "MeshInstance":
							mesh = ccc
	material[0] = mesh.get_mesh().surface_get_material(0).duplicate()
	material[1] = SpatialMaterial.new()
	randomize()
	material[1].albedo_color = Color(randf(),randf(),randf(),1)

func _debug_flash():
	mesh.set_surface_material(0,material[1])
	mesh_switch = [true, 0.2]


func _debug_flash_proc(delta):
	if !mesh_switch[0]:
		return
	if mesh_switch[1]>0:
		mesh_switch[1] -= 1*delta
	else:#normal
		mesh_switch[0] = false
		mesh.set_surface_material(0,material[0])
func _enter_tree():
	
	eye = $eye
	eye.add_exception(self)
	eye.add_exception(base.player)

	body = $body.get_child(0)


	for obj in body.get_children():
		if obj.get_class() == "AnimationTree":
			anim_tree  = obj
	anim_tree.active = true
	anim_tree["parameters/playback"].start("idle")
	state_new = statedb["chase"]
#	base.add_enemy(self)
#debug stuff
	base.last_enemy = self
	_set_debug_stuff()

func _ready():
	pass


func chase_w_path():
		dir = Vector3()
		ai_state = ai_db["follow_path"]
		timer[0] = timer[1]
		_update_path(base.player)

func proc_chase():
	dir = (base.player.transform.origin-transform.origin).normalized()
#	dir = player_dist
	dir.y = 0
	rotation.y = atan2(vel.x, vel.z)
	if vel.length() > 4:
		anim_new = ["run",false]
	else:
		anim_new = ["idle",false]

func _process(delta):
#	check_player_inview()
#	base.label.text = (str(check_player_inview()))
#	base.label.text = str(anim_tree["parameters/playback"].get_current_node())
	if state != state_new:
		state = state_new
		return
	if state == statedb["chase"]:
		proc_chase()
	elif state == statedb["onair"]:
		if is_on_floor():
			state_new = statedb["standup"]
			anim_new = ["attack-end",true]
	elif state == statedb["standup"]:
		dir = Vector3()
		if anim_tree["parameters/playback"].get_current_node() == "idle":
			state_new = statedb["chase"]
	if Input.is_action_just_pressed("debug_a"):
		phy_plug = "_jump"
	if Input.is_mouse_button_pressed(1):
		phy_plug = "_jump"

#		state_new = statedb["jump"]
#		update_player_info()
#
#		base.label.text = str(player_dist.length())
	if Input.is_action_pressed("debug_b"):
		debug_stop = false
	else:
		debug_stop = true
	_debug_flash_proc(delta)




#		phy_plug = "_jump"
#	rotation.y = atan2(vel.x, vel.z)
#	if ai_state == ai_db["chase"]:
#		proc_chase(delta)
#	elif ai_state == ai_db["unaware"]:
#		dir = player_dist.normalized()
#		ai_last_target = dir
#		if vel.length() < AI_MIN_SPEED_CHASE:
#			chase_w_path()
#	elif ai_state == ai_db["follow_path"]:
#
#		var target = path[path_cur]-global_transform.origin
#		if target.length() < 1:
#			if path_cur >= path.size()-1:#end
#				ai_state = ai_db["unaware"]
#				path_cur = 1
#			else:
#				path_cur += 1
#		dir = target.normalized()
#
#		timer[0] -= 1*delta
#		if timer[0] <= 0:
#			timer[0] = timer[1]
#			ai_state = ai_db["unaware"]
#	update_player_info()
#	check_player_inview()




	if anim_old != anim_new[0]:
		if anim_new[1]:#immediate
			anim_tree["parameters/playback"].start(anim_new[0])
		else:
			anim_tree["parameters/playback"].travel(anim_new[0])
		anim_old = anim_new[0]



#func update_player_info():
#	base.player.predict_target(self)
#	player_dist = base.player.predict_target(self)-global_transform.origin




#	var orig = global_transform.origin
#	player_dist = target



#	var target = base.player.predict_target(self)
#	var orig = global_transform.origin
#	player_dist = target





#	player_dist = Vector3(target.x-orig.x,orig.y,target.z-orig.x)
#	player_dist = base.player.predict_target(self) - global_transform.origin
#	player_dist.y = global_transform.origin.y
#	player_dist = base.player.head_pos.global_transform.origin - global_transform.origin

func check_player_inview():
	eye.enabled = false
	
#	if ai_state == ai_db["follow_path"]:
#		return
#	if eye.is_colliding():
#		ai_state = ai_db["chase"]
#	else:
#		ai_state = ai_db["unaware"]
	var playerloc = base.player.head_pos.global_transform.origin-eye.global_transform.origin
	eye.set_cast_to(playerloc)
	eye.force_raycast_update()
	var result = eye.get_collider()
#	base.label.text = str(result)
#	eye.enabled = false
	return result

func _physics_process(delta):
	
	dir.y = 0
	dir = dir.normalized()

	vel.y += delta * g
	var hvel = vel
	hvel.y = 0
	
	var target = dir * MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	if !is_on_floor():
		accel = AIR_ACCEL
	hvel = hvel.linear_interpolate(target, accel * delta)

	vel.x = hvel.x
	vel.z = hvel.z

	if phy_plug != null:
		vel = call(phy_plug, vel)
		phy_plug = null
	vel = move_and_slide(vel, Vector3(0,1,0), true)


func _jump(vel):
	if !is_on_floor():
		return vel
	if check_player_inview() != null:
		return vel
	state_new = statedb["onair"]
	anim_new = ["attack",false]

	var target = base.player.predict_target(self)-global_transform.origin
	var jump_power = target.length()*2.1
	

	vel = target.normalized()*jump_power
	rotation.y = atan2(vel.x, vel.z)
	vel.y = min(14,jump_power*.7)
	return vel
	
func _update_path(target):
#	base.counter += 1
	path_cur = 1
	var begin = global_transform.origin
	var end = target.global_transform.origin
	var test = null
	var p = base.navig.get_simple_path(begin, end, true)
	path = Array(p) # Vector3array too complex to use, convert to regular array


	if path == null:
		ai_state = ai_db["unaware"]
		return
	if path.size() < 2:
		ai_state = ai_db["unaware"]
		return
	

	if draw_path:
		var im = get_node("/root/main/draw")
		im.set_material_override(m)
		im.clear()
		im.begin(Mesh.PRIMITIVE_POINTS, null)
		im.add_vertex(begin)
		im.add_vertex(end)
		im.end()
		im.begin(Mesh.PRIMITIVE_LINE_STRIP, null)
		for x in p:
			im.add_vertex(x)
		im.end()