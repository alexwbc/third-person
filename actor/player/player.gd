extends KinematicBody
#nodes
var cam_eye
var cam_hold
var cam_move
var cam_focus
var cam_wire

var body
var skeleton = Skeleton

var anim_player
var anim_old = "idle"
var anim_new = ["idle",false]
#var anim_tree := body.get_node("blendwalk") as AnimationTree
var anim_tree

var yaw
var pitch

const phy_statedb =	{
					"onground":0
					,"falling":1
					
					}
var statedb = 	{
				"on_ground":0
				,"jump":1
				,"fall":1
				,"standup":3#link for every state that's returning on "idle" animation (on ground state)
				,"attack":4
				}
var state = 0
var state_new = 0

# phisics and movment
#var g = -9.8
var g = -40
var vel = Vector3()
var friction = false
const MAX_SPEED = 12
const JUMP_SPEED = 20

const ACCEL= 9
const DEACCEL= 9
const AIR_ACCEL = 1
const MAX_SLOPE_ANGLE = 30
var phy_plug = null#bridge between pyshics and normal process (ie: when basic process request a jump)

#services
var head_pos = Vector3(0,0,0)

#camera conf
#const PITCH_LIMIT = Vector2(-0.3,0.7)
const PITCH_LIMIT = Vector2(-0.9,1)
onready var ADJ_FOCUS = $focus.transform.origin#focus adjust for wire ray
#input
var dir = Vector3()
var mouse_speed = 0.01
var mouse_motion = Vector2(0,0)


#service
#var enemy_db = []
#var enemy_count = 0

#func update_enemy_db():
#	enemy_db.clear()
#	var size = 0
#	for e in base.enemy_list:
#		print(e)
#		size += 1
#		enemy_db.resize(size)
#		enemy_db[size-1] = e
	#base.label.text = str(enemy_db)

#func service(delta):
#	enemy_count += 1
#	if  enemy_count>=enemy_db.size():
#		enemy_count = 0
#
#	var enemy = base.enemy_list[enemy_db[enemy_count]]
#	enemy.player_dist = (global_transform.origin-enemy.global_transform.origin)*Vector3(1,0,1)

#	var updatetarget = who.global_transform.origin
#	var target = global_transform.origin
#	var predicted_target = orig-target#not actually predicted
#	updatetarget.player_dist = predicted_target
#	var length = predicted_target.length()
#	if length < 8:#if too close, no prediction will be made at all
#		target.y = orig.y
#		return target*1.2
#
#	var dist = min(12,length)/8-1
#	predicted_target = global_transform.origin+(vel*dist)
#	predicted_target.y = orig.y
#	return predicted_target






#	print(base.enemy_list[enemy_db[enemy_count]].name)
#	base.enemy_list[enemy_db[enemy_count]]._debug_flash()
#	enemy_db[enemy_count].flash()
#	var enemy_served = enemy_db[enemy_count]
#	if enemy_served == null:
#		update_enemy_db()
#		enemy_count = 0
#	else:
#		base.label.text = str(enemy_db[enemy_count])

#	base.label.text = str(base.enemy_list)




func predict_target(who =null):
	if null:
		return
#	var dist_one = who.global_transform.origin-global_transform.origin
#	var dist = (who.global_transform.origin-global_transform.origin).length()
	var orig = who.global_transform.origin
	var target = global_transform.origin
	var predicted_target = orig-target#not actually predicted
	var length = predicted_target.length()
	if length < 8:#if too close, no prediction will be made at all
		var locsign = load("res://system/tools/locsign.tscn").instance()
		locsign.color = Color(1,0,0,1)
		get_node("/root/main").add_child(locsign)
		target = target+(target-orig)
		locsign.global_transform.origin = target
		return target
	#otherwise, predict target

	var dist = min(12,length)/8-1
	predicted_target = global_transform.origin+(vel*dist)
	predicted_target.y = orig.y


	var locsign = load("res://system/tools/locsign.tscn").instance()
	locsign.color = Color(0,1,0,1)
	locsign.global_transform.origin = predicted_target
	get_node("/root/main").add_child(locsign)


	return predicted_target

	
func _enter_tree():
	if base.player != null:
		base.player.die()
	base.player = self
	cam_eye = $yaw/camera#the camera itself
	cam_hold = $yaw/pitch/cam_hold# origin position where eye/camera will be placed(when wire is not colliding)
	cam_move = $yaw/pitch/cam_hold/cam_move#actual position for camera
#	cam_focus = $focus#camera will always point towards this point
#	cam_wire = $focus/wire
#	cam_wire = $yaw/focus/wire
#	cam_focus = $yaw/focus#camera will always point towards this point
	cam_wire = $focus/wire
	cam_focus = $yaw/focus#camera will always point towards this point
	cam_wire.add_exception(self)
	
	for b in $body.get_children():
		if b.get_class() == "Spatial":
			body = b
	
#	skeleton = get_node("body/char/rig/Skeleton")
	head_pos = $headpos
	for obj in body.get_children():
		if obj.get_class() == "AnimationPlayer":
			anim_player  = obj
	if anim_player == null:
		print("ANIMATIONS NOT FOUND, DIE")
		queue_free()
	
	yaw = $yaw
	pitch = $yaw/pitch
	cam_eye.current = true
	
	
	#debug
	anim_tree = body.get_node("blendwalk") as AnimationTree
	anim_tree.active = true
	anim_tree["parameters/playback"].start("idle")


func die():
	if base.player == self:
		base.player = null
	queue_free()

#func _ready():
#	update_enemy_db()

func _input(event):
	if event.get_class() == "InputEventMouseMotion":
		mouse_motion = event.relative*mouse_speed
		var pitch_rot = min(max(PITCH_LIMIT.x,pitch.rotation.x-mouse_motion.y), PITCH_LIMIT.y)
		pitch.rotation.x = pitch_rot
		yaw.rotation.y -= mouse_motion.x
	dir = Vector3()
	var cam_xform = cam_eye.get_global_transform()
	dir += (Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward"))*cam_xform.basis[2]
	dir += (Input.get_action_strength("move_right") - Input.get_action_strength("move_left"))*cam_xform.basis[0]
	friction = dir.length()<=0.5

func pro_falling():
	var cur_anim = anim_tree["parameters/playback"].get_current_node()
	
	if is_on_floor():
		
#		if cur_anim != "falling":
		if cur_anim == "falling":
			print("falling process")
			var flat_velocity = (vel*Vector3(1,0,1)).length()
			print(str("flatv: ",flat_velocity))
			if flat_velocity <= 6:
				anim_new = ["falling-stand",true]
				state_new = statedb["standup"]
			else:
				anim_new = ["falling-run",true]
				state_new = statedb["on_ground"]

func _process(delta):
#	var cur_anim = anim_tree["parameters/playback"].get_current_node()
#	base.label.text = str(state,"\n anim: ",cur_anim)

	if state != state_new:
		state = state_new
		return
	#service(delta)
#	predict_target(base.last_enemy)
#	base.label.text = str(dir.length())
	if Input.is_action_just_pressed("debug_a"):
		anim_new = ["walk",false]
	elif Input.is_action_just_pressed("debug_b"):
		base.last_enemy._debug_flash()
	if Input.is_mouse_button_pressed(2):
		var motion = base.slow_motion_game
		motion -= 2*delta
		base.slow_motion_game = max(motion, base.SLOW_MAX)
	cam_hold.look_at(cam_focus.global_transform.origin, Vector3(0,1,0))
	cam_wire.cast_to = cam_hold.global_transform.origin-cam_wire.global_transform.origin
	var collider =  cam_wire.get_collider()
	if collider != null:
		cam_move.global_transform.origin = cam_wire.get_collision_point()
	else:
		cam_move.transform.origin = Vector3(0,0,0)

	if state == statedb["on_ground"]:
		pro_ground(delta)
	elif state == statedb["jump"]:
		pro_falling()
	elif state == statedb["standup"]:
		dir = Vector3()
		if anim_tree["parameters/playback"].get_current_node() == "idle":
			state_new = statedb["on_ground"]

#		print(anim_tree["parameters/playback"].get_current_node())
#		var cur_anim = anim_tree["parameters/playback"].get_current_node()
#		print(cur_anim)
#		if  cur_anim== "idle":
#			state_new = statedb["onground"]



	if anim_old != anim_new[0]:
		if anim_new[1]:#immediate
			anim_tree["parameters/playback"].start(anim_new[0])
		else:
			anim_tree["parameters/playback"].travel(anim_new[0])
		anim_old = anim_new[0]
	#	update_services()
#func update_services():
##	head_pos = skeleton.get_bone_global_pose(skeleton.find_bone("DEF-head")).origin+global_transform.origin
##	head_pos = skeleton.get_bone_pose(skeleton.find_bone("DEF-head")).origin+global_transform.origin
##	head_pos = skeleton.get_bone_transform(skeleton.find_bone("DEF-neck")).origin+global_transform.origin
#	head_pos = (skeleton.get_bone_global_pose(skeleton.find_bone("DEF-head")).origin*body.get_node("rig").scale)+global_transform.origin
##	head_pos = global_transform.origin
#	$testplace.global_transform.origin = head_pos
#	print(skeleton.scale)
#
func pro_ground(delta):
	if !is_on_floor():
		$feet.force_raycast_update()
		if !$feet.is_colliding():
			state_new = statedb["jump"]
			anim_new = ["falling",true]
			return
	$body.rotation.y = atan2(vel.x, vel.z)
#	var rot = vel
	
#	rot.y = 0
#	$body.look_at(rot, Vector3(0,1,0))

#	$body.rotation.y = rot.lerp(atan2(vel.x, vel.z),0.5)
#	$body.rotation.y = lerp(rot, atan2(vel.x, vel.z),0.5)
#	base.label.text = str($body.rotation.y)
#	 slerp( Basis b, float t )

	var v_length = vel.length()
	var d_length = vel.length()
	
	
	#animation control on ground
	anim_new = ["idle",true]
	if (v_length < 3) and (d_length > 0.9):#run from standing
		anim_new = ["run", false]
	elif (v_length > 3) and (d_length < 0.9):#stop running
		anim_new = ["idle", false]
	elif v_length > 3:
		anim_new[0] = "run"
	if Input.is_action_just_pressed("jump"):
		phy_plug = "_jump"
		

func phy_onground(delta):
	pass
	
func _physics_process(delta):
	dir.y = 0
	dir = dir.normalized()

	vel.y += delta * g
	var hvel = vel
	hvel.y = 0
	
	var target = dir * MAX_SPEED
	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL
	if !is_on_floor() and !$feet.is_colliding():
		accel = AIR_ACCEL
	hvel = hvel.linear_interpolate(target, accel * delta)

	vel.x = hvel.x
	vel.z = hvel.z

#	if is_on_floor():
#		if Input.is_action_pressed("jump"):
#			vel.y = JUMP_SPEED

	if phy_plug != null:
		vel = call(phy_plug, vel)
		phy_plug = null

	vel = move_and_slide(vel, Vector3(0,1,0), friction,2,0.785398,false)


func _jump(vel):
	if !is_on_floor():
		return vel
	state_new = statedb["jump"]
	if vel.length() >4:
		anim_new = ["jump-stand",true]
	else:
		anim_new = ["jump-stand",true]
	vel.y = JUMP_SPEED
#	var target = base.player.predict_target(self)-global_transform.origin
#	var jump_power = target.length()*2.1
#
#
#	vel = target.normalized()*jump_power
#	rotation.y = atan2(vel.x, vel.z)
#	vel.y = min(14,jump_power*.7)
	return vel




#func _physics_process(delta):
#	dir.y = 0
#	dir = dir.normalized()
#
#	vel.y += delta * g
#	var hvel = vel
#	hvel.y = 0
#
#	var target = dir * MAX_SPEED
#	var accel
#	if dir.dot(hvel) > 0:
#		accel = ACCEL
#	else:
#		accel = DEACCEL
#	if !is_on_floor():
#		accel = AIR_ACCEL
#	hvel = hvel.linear_interpolate(target, accel * delta)
#
#	vel.x = hvel.x
#	vel.z = hvel.z
#
#	if is_on_floor():
#		if Input.is_action_pressed("jump"):
#			vel.y = JUMP_SPEED
#
#	vel = move_and_slide(vel, Vector3(0,1,0), true)
#
#
#
#
